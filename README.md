# exercism_python

Exercism python. You can go [exercism.org](<https://exercism.org/>) to do this.

## Topics

- [ ] Finish exercises.
- [ ] Rewrite exercises' unittest by `unittest` module.

## Usage

### testing

The testing can run locally.

1. If you want to know more about tesing, see [exercism.org's testing track](<https://exercism.org/docs/tracks/python/tests>).
1. If you want to know vscode howto run testing, see [vscode's testing doc](<https://code.visualstudio.com/docs/python/testing>).
