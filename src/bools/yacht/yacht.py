# Score categories.
# Change the values as you see fit.
YACHT = 0
ONES = 1
TWOS = 2
THREES = 3
FOURS = 4
FIVES = 5
SIXES = 6
FULL_HOUSE = 7
FOUR_OF_A_KIND = 8
LITTLE_STRAIGHT = 9
BIG_STRAIGHT = 10
CHOICE = 11


def score(dice, category):
    """
    calculate score using a list of values above.
    :param dice: list
    :param category: int
    :return: int
    """
    dice.sort()
    countOne, countTwo, countThree, countFour, countFive, countSix = [0, 0, 0, 0, 0, 0]

    score = 0
    i = 0
    # match statement for dice number in 1~6.
    for dice[i] in dice:
        match dice[i]:
            case 1:
                countOne += 1
            case 2:
                countTwo += 1
            case 3:
                countThree += 1
            case 4:
                countFour += 1
            case 5:
                countFive += 1
            case 6:
                countSix += 1
        i += 1
    L = [countOne, countTwo, countThree, countFour, countFive, countSix]

    # match statement for category refer the beginning of the file.
    match category:
        case 0:
            score = 50 if (L.count(5) == 1) else 0
        case 1:
            score = L[0] if L[0] else 0
        case 2:
            score = L[1] * 2 if L[1] != 0 else 0
        case 3:
            score = L[2] * 3 if L[2] != 0 else 0
        case 4:
            score = L[3] * 4 if L[3] != 0 else 0
        case 5:
            score = L[4] * 5 if L[4] != 0 else 0
        case 6:
            score = L[5] * 6 if L[5] != 0 else 0
        case 7:
            score = (L.index(3) + 1) * 3 + (L.index(2) + 1) * 2 if (L.count(3) and L.count(2)) else 0
        case 8:
            if L.count(5) == 1 and L.index(5):
                score = (L.index(5) + 1) * 4
            else:
                if L.count(4) == 1 and L.index(4):
                    score = (L.index(4) + 1) * 4
                else:
                    score = 0
        case 9:
            score = 30 if (L == [1, 1, 1, 1, 1, 0]) else 0
        case 10:
            score = 30 if (L == [0, 1, 1, 1, 1, 1]) else 0
        case 11:
            score = sum(dice)
    return score


# second generation
COUNTABLE = [ONES, TWOS, THREES, FOURS, FIVES, SIXES]


def second_score(dice, category):
    """
    calculate score using a list of values above.
    :param dice: list
    :param category: int
    :return: int
    """
    dice.sort()
    scoreValue = 0
    maxOccurrences = max(dice, key=dice.count)
    minOccurrences = min(dice, key=dice.count)

    if category in COUNTABLE:
        scoreValue = dice.count(category) * category

    if category is LITTLE_STRAIGHT:
        if dice == [1, 2, 3, 4, 5]:
            scoreValue = 30

    if category is BIG_STRAIGHT:
        if dice == [2, 3, 4, 5, 6]:
            scoreValue = 30

    if category is CHOICE:
        scoreValue = sum(dice)

    if category is FULL_HOUSE:
        if dice.count(maxOccurrences) == 3 \
                and dice.count(minOccurrences) == 2:
            scoreValue = sum(dice)

    if category is FOUR_OF_A_KIND:
        if dice.count(maxOccurrences) >= 4:
            scoreValue = maxOccurrences * 4

    if category is YACHT:
        if maxOccurrences == minOccurrences:
            scoreValue = 50
    
    return scoreValue
