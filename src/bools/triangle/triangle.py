def equilateral(sides):
    """
    Determine if a triangle is equilateral.
    :param sides: list
    :return: bool
    """
    if IsInvalidTriangle(sides):
        isEquilateral = False
    else:
        if sides.count(sides[0]) == 3:
            isEquilateral = True
        else:
            isEquilateral = False
    return isEquilateral


def isosceles(sides):
    """
    Determine if a triangle is isosceles.
    :param sides: list
    :return: bool
    """
    if IsInvalidTriangle(sides):
        isIsosceles = False
    else:
        isIsosceles = False
        i = 0
        for sides[i] in sides:
            if sides.count(sides[i]) >= 2:
                isIsosceles = True
                break
            else:
                i += 1
    return isIsosceles


def scalene(sides):
    """
    Determine if a triangle is scalene.
    :param sides: list
    :return: bool
    """
    if IsInvalidTriangle(sides):
        isScalene = False
    else:
        if sides[0] != sides[1] \
                and sides[1] != sides[2] \
                and sides[2] != sides[0]:
            isScalene = True
        else:
            isScalene = False
    return isScalene


def IsInvalidTriangle(sides):
    if sides[0] + sides[1] < sides[2] \
            or sides[0] + sides[2] < sides[1] \
            or sides[1] + sides[2] < sides[0] \
            or sides[0] <= 0 \
            or sides[1] <= 0 \
            or sides[2] <= 0:
        isInvalidTriangle = True
    else:
        isInvalidTriangle = False
    return isInvalidTriangle
