"""Given a year, report if it is a leap year.
"""


def leap_year(year):
    """
    Given a year, report if it is a leap year.
    :param year: int
    :return: bool
    """
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                isLeapYear = True
            else:
                isLeapYear = False
        else:
            isLeapYear = True
    else:
        isLeapYear = False
    return isLeapYear
