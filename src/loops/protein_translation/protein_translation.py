def proteins(strand):
    """
    :param strand: str
    :return: list
    """

    proteinCodons = (
        ("Methionine", "AUG"),
        ("Phenylalanine", "UUU", "UUC"),
        ("Leucine", "UUA", "UUG"),
        ("Serine", "UCU", "UCC", "UCA", "UCG"),
        ("Tyrosine", "UAU", "UAC"),
        ("Cysteine", "UGU", "UGC"),
        ("Tryptophan", "UGG")
    )
    stopedCodons = ("UAA", "UAG", "UGA")

    proteinsList = []
    for idx, _ in enumerate(strand, start=1):
        if not (idx % 3):
            if strand[idx-3:idx] in stopedCodons:
                break
            for proteinCodon in proteinCodons:
                for codon in proteinCodon[1:]:
                    if strand[idx-3:idx] == codon:
                        proteinsList.append(proteinCodon[0])

    return proteinsList
