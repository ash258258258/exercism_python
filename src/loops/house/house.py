RHYMES = [
    "the malt that lay in ",
    "the rat that ate ",
    "the cat that killed ",
    "the dog that worried ",
    "the cow with the crumpled horn that tossed ",
    "the maiden all forlorn that milked ",
    "the man all tattered and torn that kissed ",
    "the priest all shaven and shorn that married ",
    "the rooster that crowed in the morn that woke ",
    "the farmer sowing his corn that kept ",
    "the horse and the hound and the horn that belonged to "
]
RAW_SENTENCE = "This is the house that Jack built."


def recite(start_verse, end_verse):
    """
    :param start_verse: int
    :param end_verse: int
    :return: str
    """

    result = []
    appendSentence = RAW_SENTENCE
    if start_verse == 1:
        result.append(appendSentence)

    for idx, item in enumerate(RHYMES, start=2):
        appendSentence = appendSentence[:8] + item + appendSentence[8:]
        if idx >= start_verse and idx <= end_verse:
            result.append(appendSentence)
        if idx == end_verse:
            break
    return result


# second, need to think of situation which beyond the range of for loop, so it's not the best approach.
def second_recite(start_verse, end_verse):
    result = []
    appendSentence = RAW_SENTENCE

    if start_verse == 1:
        result.append(appendSentence)

    for i in range(2, len(RHYMES)+2):
        appendSentence = appendSentence[:8] + RHYMES[i-2] + appendSentence[8:]
        while i >= start_verse and i <= end_verse:
            result.append(appendSentence)
            break
        if i == end_verse:
            break
        continue

    return result


# third approach that using while loop that nested in for loop.
def third_recite(start_verse, end_verse):

    result = []
    appendSentence = RAW_SENTENCE
    if start_verse == 1:
        result.append(appendSentence)

    for idx, item in enumerate(RHYMES, start=2):
        appendSentence = appendSentence[:8] + item + appendSentence[8:]
        while idx in range(start_verse, end_verse+1):
            result.append(appendSentence)
            break

    return result


# fourth approach using while loop
def fourth_recite(start_verse, end_verse):

    result = []
    appendSentence = RAW_SENTENCE
    if start_verse == 1:
        result.append(appendSentence)
        start_verse += 1

    count = 2
    while count <= len(RHYMES)+1:
        appendSentence = appendSentence[:8] + RHYMES[count-2] + appendSentence[8:]
        while count >= start_verse and count <= end_verse:
            result.append(appendSentence)
            break
        count += 1
    return result


# sixth approach by jeffdparker:
verse = [
    'Place Holder', 
    'the house that Jack built.',
    'the malt that lay in',
    'the rat that ate',
    'the cat that killed',
    'the dog that worried',
    'the cow with the crumpled horn that tossed',
    'the maiden all forlorn that milked',
    'the man all tattered and torn that kissed',
    'the priest all shaven and shorn that married',
    'the rooster that crowed in the morn that woke',
    'the farmer sowing his corn that kept',
    'the horse and the hound and the horn that belonged to',
]


def one_verse(num):
    lst = ['This is'] + [verse[val] for val in range(num, 0, -1) ]
    return ' '.join(lst)


def sixth_recite(start_verse, end_verse):
    result = [ one_verse(verb) for verb in range(start_verse, end_verse+1) ]
    return result


# seventh approach inspired by jeffdparker:
def seventh_recite(start_verse, end_verse):
    result = []
    lst = []
    for verb in range(start_verse, end_verse+1):
        lst.append('This is')
        for val in range(verb, 0, -1):
            lst.append(verse[val])
        joinLst = ' '.join(lst)
        lst.clear()
        result.append(joinLst)

    return result
