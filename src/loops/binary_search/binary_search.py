def find(search_list, value):
    if value not in search_list:
        raise ValueError("value not in array")

    index = len(search_list) // 2
    middle_value = search_list[index]
    if value > middle_value:
        index = index + 1 + find(search_list[(index+1):], value)
    if value < middle_value:
        index = find(search_list[:index], value)

    return index


# The first thinking about function, but doesn't work.
#def find(search_list, value):
#    if value not in search_list:
#        raise ValueError("value not in array")
#    searchCopy = search_list.copy()
#
#    while True:
#        mid = len(searchCopy)//2
#        if searchCopy[mid] == value:
#            value_index = mid
#        elif searchCopy[mid] < value:
#            searchCopy = searchCopy[mid-1:]
#        elif searchCopy[mid] > value:
#            searchCopy = searchCopy[:mid+1]
#        break
#
#    return value_index


# Not the best, cause it has side-effect(var i/j).
def second_find(search_list, value):
    i = 0
    j = len(search_list)-1
    while i <= j:
        mid = (i + j) // 2
        if value == search_list[mid]:
            return mid
        elif value < search_list[mid]:
            j = mid - 1
        elif value > search_list[mid]:
            i = mid + 1
        else:
            return -1   # error return code
    raise ValueError('value not in array')  # while i <= j, this raise will run.

