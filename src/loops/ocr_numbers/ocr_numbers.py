numberTuple = (
    """ _ | ||_|   """,   # 0
    """     |  |   """,   # 1
    """ _  _||_    """,   # 2
    """ _  _| _|   """,   # 3
    """   |_|  |   """,   # 4
    """ _ |_  _|   """,   # 5
    """ _ |_ |_|   """,   # 6
    """ _   |  |   """,   # 7
    """ _ |_||_|   """,   # 8
    """ _ |_| _|   """    # 9
)


def convert(input_grid):
    """
    :param input_grid: list
    :return: string
    """

    if len(input_grid) % 4:
        raise ValueError("Number of input lines is not a multiple of four")

    result = []
    for j in range(0, len(input_grid), 4):
        digits = ''  # the str type
        for i in range(0, len(input_grid[j]), 3):  # each line sperate by 3
            digit = []
            for a in range(4):  # the distance between line j and now.
                if len(input_grid[j+a]) % 3:
                    raise ValueError("Number of input columns is not a multiple of three")
                digit.append(input_grid[j+a][i:i+3])  # slice line of [i:i+3].
            digitNumber = "".join(digit) # the 3*4 binary font.
            if digitNumber in numberTuple:
                digits += str(numberTuple.index(digitNumber)) 
            else:
                digits += '?'
        result.append(digits)
    concateEach34 = ",".join(result)
    return concateEach34
