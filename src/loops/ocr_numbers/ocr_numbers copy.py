numberTuple = (
    """\n _ \n| |\n|_|\n   \n""",
    """\n   \n  |\n  |\n   \n""",
    """\n _ \n _|\n|_ \n   \n""",
    """\n _ \n _|\n _|\n   \n""",
    """\n   \n|_|\n  |\n   \n""",
    """\n _ \n|_ \n _|\n   \n""",
    """\n _ \n|_ \n|_|\n   \n""",
    """\n _ \n  |\n  |\n   \n""",
    """\n _ \n|_|\n|_|\n   \n""",
    """\n _ \n|_|\n _|\n   \n"""
)


def convert(input_grid):
    """
    :param input_grid: list
    :return: string
    """

    if len(input_grid) % 4 != 0:
        raise ValueError("Number of input lines is not a multiple of four")

    threeList = []
    splitList = []

    for dx, item in enumerate(input_grid, start=1):
        if len(item) % 3 != 0:
            raise ValueError("Number of input columns is not a multiple of three")

        for idx, i in enumerate(item, start=1):
            if idx % 3 == 0:
                splitList.append(item[idx-3:idx])
        threeList.append(splitList)
        splitList = []
        if dx % 4 == 0 and dx != len(input_grid):
            break
    transposedThreeList = list(map(list, zip(*threeList)))
    ocrNumber = ''

    for innerList in transposedThreeList:
        x = '\n'.join(innerList)
        eachNumber = f"\n{x}\n"
        if eachNumber in numberTuple:
            ocrNumber += str(numberTuple.index(eachNumber))
        else:
            ocrNumber += '?'
        #if len(innerList) !=
        
    return ocrNumber
