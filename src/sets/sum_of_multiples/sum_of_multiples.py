def sum_of_multiples(limit, multiples):
    """
    :param limit: int
    :param multiples: list
    :return: int
    """

    sets = []
    for item in multiples:
        each_set = set()
        if item:
            for it in range(item, limit, item):
                each_set.add(it)
        sets.append(each_set)
    
    unionSet = set()
    for i in sets:
        unionSet = unionSet.union(i)

    return sum(unionSet)
