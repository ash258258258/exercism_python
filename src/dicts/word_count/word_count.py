import string
import re
from collections import Counter


def count_words(sentence):
    # senten = sentence.lower().replace('\n', ' ').replace('\t', ' ')

    sentenceListWithoutWhitespace = []
    for idx, i in enumerate(list(sentence)):

        # replace the '\t', '\n' to ' '.
        if i in string.whitespace:
            sentenceListWithoutWhitespace.append(' ')
        elif i in string.punctuation:

            # append a contraction of two simple words joined by a single apostrophe.
            if i == "'" \
                    and idx in range(1, len(sentence)-1) \
                    and sentence[idx-1] in string.ascii_letters \
                    and sentence[idx+1] in string.ascii_letters:
                sentenceListWithoutWhitespace.append(i)

            # replace the other punctuation to ' '.
            else:
                sentenceListWithoutWhitespace.append(' ')

        # if `i` is numeric or letter.
        elif i in string.ascii_letters or string.digits:
            sentenceListWithoutWhitespace.append(i.lower())

    sentenceRemoveTabAndNewline = ''.join(sentenceListWithoutWhitespace)

    sentenceL = sentenceRemoveTabAndNewline.split()
    sentenceD = {}
    for item in sentenceL:
        if item in sentenceD:
            sentenceD[item] += 1
        else:
            sentenceD[item] = 1

    return sentenceD


def second_count_words(sentence):
    result = re.findall(r"[a-z]+'{1}[a-z]+|[a-z0-9]+", string=sentence.lower())

    counterDict = {}
    for item in result:
        if item not in counterDict:
            counterDict[item] = 1
        else:
            counterDict[item] += 1
    return counterDict


def third_count_words(sentence):
    return Counter(re.findall(r"[a-z]+'{1}[a-z]+|[a-z0-9]+", string=sentence.lower()))
