def transform(legacy_data):
    """
    :param legacy_data: dict
    :return: dict
    """

    transDict = {}
    for key, letterList in legacy_data.items():
        for letter in legacy_data[key]:
            letterLow = letter.lower()
            transDict[letterLow] = key

    return transDict
