spaces_earth_year = {
    "Mercury": 0.2408467,
    "Venus": 0.61519726,
    "Earth": 1.0,  # 365.25 days
    "Mars": 1.8808158,
    "Jupiter": 11.862615,
    "Saturn": 29.447498,
    "Uranus": 84.016846,
    "Neptune": 164.79132
}

days_each_earth_year = 365.25
seconds_each_earth_year = 31557600

class SpaceAge:
    def __init__(self, seconds):
        self.seconds = seconds
        self.earth_years = seconds / 31557600
    
    def on_earth(self):
        earth_year = round(self.earth_years, ndigits=2)
        return earth_year

    def on_mercury(self):
        mercury_year = round(self.earth_years / spaces_earth_year["Mercury"], ndigits=2)
        return mercury_year

    def on_venus(self):
        venus_year = round(self.earth_years / spaces_earth_year["Venus"], ndigits=2)
        return venus_year

    def on_mars(self):
        mars_year = round(self.earth_years / spaces_earth_year["Mars"], ndigits=2)
        return mars_year

    def on_jupiter(self):
        jupiter_year = round(self.earth_years / spaces_earth_year["Jupiter"], ndigits=2)
        return jupiter_year

    def on_saturn(self):
        saturn_year = round(self.earth_years / spaces_earth_year["Saturn"], ndigits=2)
        return saturn_year

    def on_uranus(self):
        uranus_year = round(self.earth_years / spaces_earth_year["Uranus"], ndigits=2)
        return uranus_year

    def on_neptune(self):
        neptune_year = round(self.earth_years / spaces_earth_year["Neptune"], ndigits=2)
        return neptune_year