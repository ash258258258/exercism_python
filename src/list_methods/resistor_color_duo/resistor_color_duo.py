def value(colors):
    """
    Input the color name, output a two digit number, even if the input is more than two colors.
    :param colors: list
    :return: int
    """

    colorValue = ("black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white")

    stredIntList = []
    for color in colors[:2]:
        if color in colorValue:
            stredIntList.append(str(colorValue.index(color)))

    colorInt = int(''.join(stredIntList))
    return colorInt
