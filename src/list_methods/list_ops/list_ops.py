def append(list1, list2):
    """
    :param list1: list
    :param list2: list
    :return: list
    """
    list1.extend(list2)
    return list1


def second_append(list1, list2):
    for l2 in list2:
        list1.append(l2)
    return list1


def concat(lists):
    """
    :param lists: list
    :return: list
    """

    flattedList = []
    for li in lists:
        append(flattedList, li)

    return flattedList


def second_concat(lists):
    """
    :param lists: list
    :return: list
    """

    flattedList = []
    for items in lists:
        if isinstance(items, list):
            for item in items:
                flattedList.append(item)
        else:
            flattedList.append(items)

    return flattedList


def filter(function, list):
    """
    :param function: func
    :param list: list
    :return: list
    """

    funcList = []
    for item in list:
        if function(item):
            funcList.append(item)

    return funcList


def length(list):
    """
    :param list: list
    :return: int
    """

    leng = 0
    for item in list:
        if item:
            leng += 1

    return leng


def map(function, list):
    """
    :param function: func
    :param list: list
    :return: list
    """

    mapList = []
    for item in list:
        mapList.append(function(item))

    return mapList


def foldl(function, list, initial):
    """
    :param function: func
    :param list: list
    :param initital: int
    :return: list
    """

    for item in list:
        initial = function(item, initial)
    return initial


def foldr(function, list, initial):
    """
    :param function: func
    """

    for item in list[::-1]:
        initial = function(item, initial)

    return initial


def reverse(list):
    """
    :param list: list
    :return: list
    """

    reverseList = []
    while list:
        pop = list.pop()
        reverseList.append(pop)
    return reverseList


def second_reverse(list):
    reverseList = list[::-1]
    return reverseList
