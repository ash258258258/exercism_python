def square(number):
    if number not in range(1, 65):
        raise ValueError("square must be between 1 and 64")
    return 2 ** (number - 1)


def total():
    return (1 - 2 ** 64) // (1 - 2)


## 2nd approach: pow() function
#
# def square(number):
# if number not in range(1, 65):
#     raise ValueError("square must be between 1 and 64")
# return pow(2, number-1)
#
# def total():
#     return (1 - pow(2, 64)) // (1 - 2)


## 3rd approach: bit shifting
#
# def square(number):
# if number not in range(1, 65):
#    raise ValueError("square must be between 1 and 64")
# return 1 << number - 1
#
# def total():
#     return (1 << 64) - 1
