"""
Diffie-Hellman key exchange.
"""
import random


def private_key(p):
    """
    Select a prime number to be private key between 1 and p.
    :param p: int
    :return: int
    """
    privateKey = random.randrange(2, p)
    return privateKey


def public_key(p, g, private):
    """
    Using private key to generate public key.
    :param p: int
    :param g: int
    :param private: int
    :return: int
    """
    publicKey = pow(g, private, p)
    return publicKey


def secret(p, public, private):
    """
    :param p: int
    :param public: int
    :param private: int
    :return: int
    """
    sec = pow(public, private, p)
    return sec
