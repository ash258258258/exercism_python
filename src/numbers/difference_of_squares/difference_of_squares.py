"""About the Sum Square Difference.
   More info on:<https://brilliant.org/wiki/sum-of-n-n2-or-n3/>
"""


def square_of_sum(number):
    squareOfSum = ((1 + number) * number / 2) ** 2
    return squareOfSum


def sum_of_squares(number):
    """
    :param number: int - the maxium number of all.
    :return: int - the sum of anyone number's square.
    """
    sumOfSquares = (number * (number + 1) * (2 * number + 1) / 6)
    # = (number * (number+1) * (2*number+1) / (2*3))
    return sumOfSquares


def difference_of_squares(number):
    """
    :param number: int - the maxium number of all.
    :return: int - The difference value betwwen square of sum and sum of squares.
    """
    return square_of_sum(number) - sum_of_squares(number)


# another approach:
def another_sum_of_squares(n: int) -> int:
    return sum(map(lambda x: x**2, range(1, n + 1)))


def another_square_of_sum(n: int) -> int:
    return sum(range(1, n + 1)) ** 2


def another_difference_of_squares(n: int) -> int:
    return another_square_of_sum(n) - another_sum_of_squares(n)
