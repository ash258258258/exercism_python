"""
The Collatz Conjecture a.k.a. 3x+1 problem.
"""


def steps(number):
    """
    :param number: int
    :return: int
    """
    count = 0
    if number <= 0:
        raise ValueError("Only positive integers are allowed")
    elif number == 1:
        return count
    while number != 1:
        if number % 2 == 0:
            number = number / 2
            count += 1
        elif number % 2 == 1:
            number = 3 * number + 1
            count += 1
        else:
            raise ValueError(f"when count={count}, number={number}, the program was error.")
    return count


# Ternary operator
def second_steps(n):
    """
    :param n: int
    :return: int
    """
    if n <= 0:
        raise ValueError("Only positive integers are allowed")
    count = 0
    while n != 1:
        n = 3 * n + 1 if n % 2 == 1 else n / 2
        count += 1
    return count


# If/else
def third_steps(n):
    """
    :param n: int
    :return: int
    """
    if n <= 0:
        raise ValueError("Only positive integers are allowed")
    count = 0
    while n != 1:
        if n % 2 == 0:
            n /= 2
        else:
            n = n * 3 + 1
        count += 1
    return count


# Recursive function
def fourth_steps(n):
    """
    :param n: int
    :return: int
    """
    if n <= 0:
        raise ValueError("Only positive integers are allowed")
    if n == 1:
        return 0
    n = n / 2 if n % 2 == 0 else n * 3 + 1
    return 1 + steps(n) # `steps(n)` need changes function name to `steps`!
