"""
Function to find the armstrong number.
An Armstrong number is a number that is the sum of its own digits each raised to the power of the number of digits.
"""


def is_armstrong_number(number):
    """

    :param number: int
    :return: bool
    """

    perDigitList = []
    numberOrigin = number
    numberLength, numberLengthOrigin = len(str(number)), len(str(number))
    while numberLength >= 0:
        perDigit = number // (10 ** numberLength)
        perDigitList.append(perDigit)
        number = number - perDigit * (10 ** numberLength)
        numberLength = numberLength - 1
    powDigitList = []
    for i in perDigitList:
        i = pow(i, numberLengthOrigin)
        powDigitList.append(i)
    if sum(powDigitList) == numberOrigin:
        isArmstrongNumber = True
    else:
        isArmstrongNumber = False
    return isArmstrongNumber


def ng_is_armstrong_number(n: int) -> bool:
    """
    :param n: int
    :return: bool
    """
    nToStr = str(n)
    strLens = len(nToStr)
    nToStrList = list(nToStr)
    digitPowSum = 0
    for digit in nToStrList:
        digitPowSum = digitPowSum + pow(digit, strLens)
    if digitPowSum == n:
        isArmstrongNumber = True
    else:
        isArmstrongNumber = False
    return isArmstrongNumber
