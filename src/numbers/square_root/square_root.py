"""
Given a natural radicand, return its square root.
(without built-ins or such as `math` modules)
"""

def square_root(number):
    """
    :param number: int
    :return: int
    """

    if number < 0:
        raise ValueError("The number should be positive.")
    
    i = 0
    while i ** 2 < number:
        i += 1
    return i


def second_square_root(number):
    for i in range(number):
        if number == 1:
            return 1
        if i * i == number:
            return i


# Using bitwise operators
def third_square_root(number):
    c = 0
    d = 1 << 30

    while d:
        if number >= c + d:
            number -= c + d
            c = (c >> 1) + d
        else:
            c >>= 1
        d >>= 2
    return c