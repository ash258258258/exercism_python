"""
Implement a program that translates from English to Pig Latin.
"""


def translate(text):
    """
    :param text: str
    :return: str
    """
    # cause str maybe is many words between space, split it to each word in list
    textCopys = text.split()
    # empty list to fill in translated words
    textCopyed = []
    for textCopy in textCopys:
        # For words that begin with vowel sounds
        if textCopy.startswith(('a', 'e', 'i', 'o', 'u')) \
                or textCopy.startswith(('xr', 'yt')):
            textCopy = textCopy + 'ay'
        # For words that begin with consonant sounds(and y can consonant sounds)
        else:
            # begin with y
            if textCopy.startswith('y'):
                textCopy = textCopy[1:] + textCopy[0] + 'ay'
            # begin with q
            elif textCopy.startswith('q'):
                # begin with qu
                if textCopy.startswith(('qu')):
                    textCopy = textCopy[2:] + textCopy[0:2] + 'ay'
                else:
                    textCopy = textCopy[1:] + textCopy[0] + 'ay'
            # begin with consonant sounds, next with qu
            elif textCopy[1:3] == 'qu':
                textCopy = textCopy[3:] + textCopy[0:3] + 'ay'
            # begin with thr or sch(3 letter)
            elif textCopy.startswith(('thr', 'sch')):
                textCopy = textCopy[3:] + textCopy[0:3] + 'ay'
            # begin with ch or th(2 letter)
            elif textCopy.startswith(('ch', 'th')):
                textCopy = textCopy[2:] + textCopy[0:2] + 'ay'
            # begin with consonant sounds, next with y
            elif textCopy.startswith(('rhy')):
                textCopy = textCopy[2:] + textCopy[0:2] + 'ay'
            # others that begin with consonant sounds
            else:
                textCopy = textCopy[1:] + textCopy[0] + 'ay'
        # Add to list
        textCopyed.append(textCopy)

    # convert list to str
    result = ' '.join(textCopyed)
    return result


# Second generation
VOWELS = {"a", "e", "i", "o", "u"}
VOWELS_Y = {"y", "a", "e", "i", "o", "u"}
SPECIALS = {"xr", "yt"}


def second_translate(text):
    """
    :param text: str
    :return: str
    """
    pigLatinList = []
    for word in text.split():
        if word[0] in VOWELS or word[0:2] in SPECIALS:
            pigLatinList.append(word + "ay")
            continue

        # traverse word except first letter
        for pos in range(1, len(word)):
            if word[pos] in VOWELS_Y:
                if word[pos] == 'u' and word[pos - 1] == 'q':
                    pos += 1
                pigLatinList.append(word[pos:] + word[:pos] + "ay")
                break

    pigLatin = " ".join(pigLatinList)
    return pigLatin
