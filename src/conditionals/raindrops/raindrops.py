def convert(number):
    """
    :param number: int
    :return: str
    """
    isRaindrops = ''
    if number % 3 == 0:
        isRaindrops += 'Pling'
    if number % 5 == 0:
        isRaindrops += 'Plang'
    if number % 7 == 0:
        isRaindrops += 'Plong'
    if isRaindrops == '':
        isRaindrops = str(number)
    return isRaindrops
