def is_paired(input_string):
    """
    Given a string containing brackets [], braces {}, parentheses (), or any combination thereof, verify that any and all pairs are matched and nested correctly.
    :param input_string: str
    :return: bool
    """

    beginPair = ['(', '[', '{']
    endPair = [')', ']', '}']
    stack = []

    for char in input_string:
        if char in beginPair:
            stack.append(char)
        elif char in endPair:
            if not stack or beginPair[endPair.index(char)] != stack.pop():
                isPaired = False
                return isPaired
        else:
            continue
    if not stack:
        isPaired = True
    else:
        isPaired = False
    return isPaired
