def response(hey_bob):
    """
    :param hey_bob: str
    :return: str
    """
    heyBob = hey_bob.strip()
    if heyBob.endswith('?') \
            and not heyBob.isupper():
        bobResponse = "Sure."
    elif heyBob.isupper() \
            and not heyBob.endswith('?'):
        bobResponse = "Whoa, chill out!"
    elif heyBob.isupper() \
            and heyBob.endswith('?'):
        bobResponse = "Calm down, I know what I'm doing!"
    elif heyBob.strip() == '':
        bobResponse = "Fine. Be that way!"
    else:
        bobResponse = "Whatever."

    return bobResponse


# if statements
def second_response(hey_bob):
    """
    :param hey_bob: str
    :return: str
    """
    # rstrip() makes ends space equal or less than origin. 
    hey_bob = hey_bob.rstrip()
    bobRes = "Whatever."
    if not hey_bob:
        bobRes = "Fine. Be that way!"
    is_shout = hey_bob.isupper()
    is_question = hey_bob.endswith("?")
    if is_shout:
        bobRes = "Whoa, chill out!"
    if is_question:
        bobRes = "Sure."
    if is_shout and is_question:
        bobRes = "Calm down, I know what I'm doing!"

    return bobRes


# if statements nested
def third_response(hey_bob):
    hey_bob = hey_bob.rstrip()
    bobRes = "Whatever."
    if not hey_bob:
        bobRes = "Fine. Be that way!"
    is_shout = hey_bob.isupper()
    is_question = hey_bob.endswith("?")
    if is_question:
        bobRes = "Sure."
    if is_shout:
        bobRes = "Whoa, chill out!"
        if is_question:
            bobRes = "Calm down, I know what I'm doing!"

    return bobRes
