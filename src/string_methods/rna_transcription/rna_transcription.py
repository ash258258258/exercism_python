# Using hashmap
def to_rna(dna_strand):
    """
    :param dna_strand: str
    :return: str
    """

    dna2rna = {'G': 'C', 'C': 'G', 'T': 'A', 'A': 'U'}
    rna_strand = ""
    for dna in dna_strand:
        rna_strand += dna2rna[dna]
    
    return rna_strand


# Using str.translate
def second_to_rna(dna_strand):
    LOOKUP = str.maketrans("GCTA", "CGAU")
    return dna_strand.translate(LOOKUP)