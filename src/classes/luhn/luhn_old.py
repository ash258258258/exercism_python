class Luhn:
    def __init__(self, card_num):
        """
        :param card_num: str
        """

        self.card_num = card_num
        pass

    def valid(self):
        """
        validating a number.
        :param:
        :return:
        """

        card_num = self.card_num.replace(" ", "")
        if len(card_num) <= 1:
            return False

        new_card_num = []
        appenden_digit = 0
        sum_all_digits = 0
        for idx, digit in enumerate(reversed(card_num) ,start=1):
            if digit not in '0123456789':
                return False

            if not idx % 2:
                if int(digit) >= 5:
                    appenden_digit = int(digit) * 2 - 9
                    new_card_num.append(str(appenden_digit))
                else:
                    appenden_digit = int(digit) * 2
                    new_card_num.append(appenden_digit)
            else:
                appenden_digit = int(digit)
                new_card_num.append(appenden_digit)

            sum_all_digits += appenden_digit

        if not sum_all_digits % 10:
            return True
        elif sum_all_digits % 10:
            return False
