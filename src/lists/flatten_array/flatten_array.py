def S_flatten(iterable):
    """
    :param iterable: list
    :return: list
    """

    flattenList = []
    # Use recursion
    for item in iterable:
        if isinstance(item, list):
            flattenList += flatten(item)

        if isinstance(item, int):
            flattenList.append(item)

    return flattenList


def flatten(array):
    result = []
    for x in array:
        if isinstance(x, list):
            result.extend(flatten(x))
        elif x is not None:
            result.append(x)
    return result
