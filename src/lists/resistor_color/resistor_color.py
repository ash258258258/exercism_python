def color_code(color):
    """
    A function to look up the numerical value associated with a particular color band.
    :param color: str
    :return: int
    """

    colorValue = colors().index(color)
    return colorValue


def colors():
    """
    A function to list the different band colors.
    :return: list
    """

    colorList = ["black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"]
    return colorList


# second
from enum import Enum


class ResistorColor(Enum):
    black = 0
    brown = 1
    red = 2
    orange = 3
    yellow = 4
    green = 5
    blue = 6
    violet = 7
    grey = 8
    white = 9

    @classmethod
    def colors(cls):
        colorList = []
        for name, member in ResistorColor.__members__.items():
            colorList.append(name)
        return colorList


import unittest


class ResistorColorEnumTest(unittest.TestCase):

    def testBlack(self):
        self.assertEqual(ResistorColor.black.value, 0)

    def testWhite(self):
        self.assertEqual(ResistorColor.white.value, 9)

    def testOrange(self):
        self.assertEqual(ResistorColor.orange.value, 3)

    def testColors(self):
        expected = [
            "black",
            "brown",
            "red",
            "orange",
            "yellow",
            "green",
            "blue",
            "violet",
            "grey",
            "white",
        ]

        self.assertEqual(ResistorColor.colors(), expected)
