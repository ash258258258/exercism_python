"""
Roman numeral written by expressing each digit separately starting with the left most digit and skipping any digit with a value of zero.
The maximum number supported by this notation is 3,999.
1 => I
5 => V
7 => VII
10 => X
575 => DLXXV
666 => DCLXVI (600 => )
1990 => MCMXC (1000 => M, 900 = CM, 90 => XC)
2008 => MMVIII (2000 => MM, 8 => VIII)
"""


def f_roman(number):
    """
    :param number: int
    :return: str
    """

    # The result of converted roman number from int number.
    romanNumber = ""

    # roman numberals only support maxium 3999.
    if number > 3999:
        raise ValueError("Out of roman numerals range, number maxium is 3999.")

    numberToStrTuple = tuple(str(number))

    for idx, item in enumerate(reversed(numberToStrTuple), start=1):
        item = int(item)
        if item == 0:
            continue
        elif item == 9:
            if idx == 1:
                romanNumber = 'IX'
            elif idx == 2:
                romanNumber = 'XC' + romanNumber
            elif idx == 3:
                romanNumber = 'CM' + romanNumber
        elif 5 <= item <= 8:
            if idx == 1:
                romanNumber = 'V' + (item-5) * 'I'
            elif idx == 2:
                romanNumber = 'L' + (item-5) * 'X' + romanNumber
            elif idx == 3:
                romanNumber = 'D' + (item-5) * 'C' + romanNumber
        elif item == 4:
            if idx == 1:
                romanNumber = 'IV'
            elif idx == 2:
                romanNumber = 'XL' + romanNumber
            elif idx == 3:
                romanNumber = 'CD' + romanNumber
        elif 1 <= item <= 3:
            if idx == 1:
                romanNumber = item * 'I'
            elif idx == 2:
                romanNumber = item * 'X' + romanNumber
            elif idx == 3:
                romanNumber = item * 'C' + romanNumber
            elif idx == 4:
                romanNumber = item * 'M' + romanNumber

    return romanNumber


# second approch
# Use a translation table
numbers = [1000, 900, 500, 400, 100, 90, 50,  40,  10,  9,   5,   4,   1]
names   = [ 'M', 'CM','D','CD', 'C','XC','L','XL', 'X','IX','V','IV', 'I']


def second_roman(number):
    "Take a decimal number and return Roman Numeral Representation"

    # List of roman symbols
    res = []

    while (4000 > number > 0):
        # Find the largest amount we can chip off
        for idx, item in enumerate(numbers):
            if (number >= item):
                res.append(names[idx])
                number = number - item
                break
    return "".join(res)


# third approach
VALUE_SYMBOLS = [
    (1000, 'M'),
    (900, 'CM'),
    (500, 'D'),
    (400, 'CD'),
    (100, 'C'),
    (90, 'XC'),
    (50, 'L'),
    (40, 'XL'),
    (10, 'X'),
    (9, 'IX'),
    (5, 'V'),
    (4, 'IV'),
    (1, 'I')
]


def roman(number):

    romanNumber = ""
    remain = number

    for val, symbol in VALUE_SYMBOLS:
        while remain >= val:
            romanNumber = romanNumber + symbol
            remain = remain - val

    return romanNumber
