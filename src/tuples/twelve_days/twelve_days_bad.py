def recite(start_verse, end_verse):
    """
    :param start_verse: int
    :param end_verse: int
    :return: list
    """
    dayStrList = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth"]

    cumulativeSong = []
    afterColonStrList = []
    beforeColonStr = "On the _ day of Christmas my true love gave to me:"
    increasingStrTuple = (
        " a Partridge in a Pear Tree",
        " two Turtle Doves",
        " three French Hens",
        " four Calling Birds",
        " five Gold Rings",
        " six Geese-a-Laying",
        " seven Swans-a-Swimming",
        " eight Maids-a-Milking",
        " nine Ladies Dancing",
        " ten Lords-a-Leaping",
        " eleven Pipers Piping",
        " twelve Drummers Drumming"
    )

    begin = start_verse - 1
    startsWithStr = ""
    count = 0
    for item in increasingStrTuple:
        if start_verse == end_verse:
            if count == 1:
                break
        elif start_verse != end_verse:
            if count == end_verse - start_verse:
                break

        startsWithStr = beforeColonStr.replace("_", dayStrList[begin])
        cumulativeSong.append(startsWithStr)
        startsWithStr = ""
        for i in range(begin, -1, -1):
            if i:
                afterColonStrList.append(increasingStrTuple[i] + ",")
            if not i:
                if begin == 0:
                    afterColonStrList.append(increasingStrTuple[i] + ".")
                else:
                    afterColonStrList.append(" and" + increasingStrTuple[i] + ".")
        begin = begin + 1
        cumulativeSong.extend(afterColonStrList)
        afterColonStrList = []
        count += 1
    x = []
    x.append("".join(cumulativeSong))
    return x
