theTwelveDaysofChristmas = (
    ("first", "a Partridge ",),
    ("second", "two Turtle Doves, "),
    ("third", "three French Hens, "),
    ("fourth", "four Calling Birds, "),
    ("fifth", "five Gold Rings, "),
    ("sixth", "six Geese-a-Laying, "),
    ("seventh", "seven Swans-a-Swimming, "),
    ("eighth", "eight Maids-a-Milking, "),
    ("ninth", "nine Ladies Dancing, "),
    ("tenth", "ten Lords-a-Leaping, "),
    ("eleventh", "eleven Pipers Piping, "),
    ("twelfth", "twelve Drummers Drumming, ")
)

beginningSentence = "On the first day of Christmas my true love gave to me: "
endingSentence = "in a Pear Tree."


def recite(start_verse, end_verse):
    """
    :param start_verse: int
    :param end_verse: int
    :return: list
    """

    afterSentence = ""
    result = []
    for idx, item in enumerate(theTwelveDaysofChristmas):
        nowBeginSentence = f"On the {theTwelveDaysofChristmas[idx][0]} day of Christmas my true love gave to me: "

        if idx == 1:
            afterSentence = 'and ' + afterSentence
        afterSentence = theTwelveDaysofChristmas[idx][1] + afterSentence
        while idx+1 in range(start_verse, end_verse+1):
            result.append(nowBeginSentence + afterSentence + endingSentence)
            break
    return result
