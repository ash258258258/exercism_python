def f_recite(start_verse, end_verse):
    """
    :param start_verse: int
    :param end_verse: int
    :return: list
    """
    days = ("first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth")

    resultSentence = []
    choruses = (
        "twelve Drummers Drumming, ",
        "eleven Pipers Piping, ",
        "ten Lords-a-Leaping, ",
        "nine Ladies Dancing, ",
        "eight Maids-a-Milking, ",
        "seven Swans-a-Swimming, ",
        "six Geese-a-Laying, ",
        "five Gold Rings, ",
        "four Calling Birds, ",
        "three French Hens, ",
        "two Turtle Doves, ",
        "a Partridge in a Pear Tree."
    )
    (
        "a Partridge in a Pear Tree.",
        "two Turtle Doves, ",
        "three French Hens, ",
        "four Calling Birds, ",
        "five Gold Rings, ",
        "six Geese-a-Laying, ",
        "seven Swans-a-Swimming, ",
        "eight Maids-a-Milking, ",
        "nine Ladies Dancing, ",
        "ten Lords-a-Leaping, ",
        "eleven Pipers Piping, ",
        "twelve Drummers Drumming, "
    )

    for day in range(start_verse, end_verse + 1):
        sentence = f"On the {days[day-1]} day of Christmas my true love gave to me: "
        #chorus = "".join(choruses[len(choruses)-day:len(choruses)])
        #resultSentence.append(sentence + chorus)
        chorus = ''.join(choruses[day:0:-1])
        chorus_end = 'and ' + choruses[0] if day else choruses[0]
        resultSentence.append(sentence + chorus + chorus_end)
    return resultSentence

def recite(start_verse, end_verse):
    days = ('first',
            'second',
            'third',
            'fourth',
            'fifth',
            'sixth',
            'seventh',
            'eighth',
            'ninth',
            'tenth',
            'eleventh',
            'twelfth')
    choruses = ('twelve Drummers Drumming, ',
                'eleven Pipers Piping, ',
                'ten Lords-a-Leaping, ',
                'nine Ladies Dancing, ',
                'eight Maids-a-Milking, ',
                'seven Swans-a-Swimming, ',
                'six Geese-a-Laying, ',
                'five Gold Rings, ',
                'four Calling Birds, ',
                'three French Hens, ',
                'two Turtle Doves, and ',
                'a Partridge in a Pear Tree.')
    result = []
    for day in range(start_verse, end_verse+1):
        sentence = f'On the {days[day-1]} day of Christmas my true love gave to me: '
        chorus = ''.join(choruses[len(choruses)-day:len(choruses)])
        result.append(sentence + chorus)
    return result
