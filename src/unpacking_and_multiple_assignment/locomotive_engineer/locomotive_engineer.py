"""Functions which helps the locomotive engineer to keep track of the train."""


def get_list_of_wagons(*a_wagons_list):
    """Return a list of wagons.

    :param: arbitrary number of wagons.
    :return: list - list of wagons.
    """
    
    *wagonList, = a_wagons_list
    return wagonList


def fix_list_of_wagons(each_wagons_id, missing_wagons):
    """Fix the list of wagons.

    :parm each_wagons_id: list - the list of wagons.
    :parm missing_wagons: list - the list of missing wagons.
    :return: list - list of wagons.
    """
    
    x, y, z, *rest = each_wagons_id
    *sorted_wagons, = z, *missing_wagons, *rest, x, y

    return sorted_wagons


def add_missing_stops(routing_dict, **kwargs):
    """Add missing stops to route dict.

    :param route: dict - the dict of routing information.
    :param: arbitrary number of stops.
    :return: dict - updated route dictionary.
    """

    *args, = kwargs.values()
    result_dict = {**routing_dict, "stops": args}

    return result_dict


def extend_route_information(route, more_route_information):
    """Extend route information with more_route_information.

    :param route: dict - the route information.
    :param more_route_information: dict -  extra route information.
    :return: dict - extended route information.
    """

    result = {**route, **more_route_information}

    return result


def fix_wagon_depot(wagons_rows):
    """Fix the list of rows of wagons.

    :param wagons_rows: list[list[tuple]] - the list of rows of wagons.
    :return: list[list[tuple]] - list of rows of wagons.
    """

    [*a], [*b], [*c] = zip(*wagons_rows)
    result = [a, b, c]

    return result