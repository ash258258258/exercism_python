def proverb(*inputs, **kwargs):
    """
    :param inputs: list
    :param kwargs: dict
    :return: list
    """

    sentences = []
    count = 0
    originBegin = ""
    while inputs:        
        x, *inputs = inputs

        if not count:
            originBegin = x
            count += 1
        
        if not inputs:
            if originBegin and kwargs['qualifier'] == None:
                sentences.append(f"And all for the want of a {originBegin}.")
            elif originBegin and kwargs['qualifier'] != None:
                sentences.append(f"And all for the want of a {kwargs['qualifier']} {originBegin}.")
            elif not originBegin and kwargs["qualifier"] != None:
                sentences.append(f"And all for the want of a {kwargs['qualifier']}.")
            break
        sentences.append(f"For want of a {x} the {inputs[0]} was lost.")

    return sentences 

# Using position argument:
def othersArgument_proverb(*inputs, qualifier=None):
    """
    :param inputs: list
    :param qualifier: str
    :return: list
    """

    sentences = []
    count = 0
    originBegin = ""
    while inputs:        
        x, *inputs = inputs

        if not count:
            originBegin = x
            count += 1
        
        if not inputs:
            if originBegin and qualifier == None:
                sentences.append(f"And all for the want of a {originBegin}.")
            elif originBegin and qualifier != None:
                sentences.append(f"And all for the want of a {qualifier} {originBegin}.")
            elif not originBegin and qualifier != None:
                sentences.append(f"And all for the want of a {qualifier}.")
            break
        sentences.append(f"For want of a {x} the {inputs[0]} was lost.")

    return sentences 