"""
The f-string exercism.
"""


def two_fer(name='you'):
    """
    param: name -> string
    return: -> string
    """
    text = f'One for {name}, one for me.'
    return text
