# Using dict
def score(word):
    """
    :param word: str
    :return: int, the score of word.
    """

    letter_map = {
        'A': 1, 'E': 1, 'I': 1, 'O': 1, 'U': 1, 'L': 1, 'N': 1, 'R': 1, 'S': 1, 'T': 1,
        'D': 2, 'G': 2,
        'B': 3, 'C': 3, 'M': 3, 'P': 3,
        'F': 4, 'H': 4, 'V': 4, 'W': 4, 'Y': 4,
        'K': 5,
        'J': 8, 'X': 8,
        'Q': 10, 'Z': 10
    }
    letter_score = 0
    word_upper = word.upper()

    for letter in word_upper:
        if letter in letter_map.keys():
            letter_score +=letter_map[letter]
    
    return letter_score


# Using enum
from enum import IntEnum


class Scrabble(IntEnum):
    A = E = I = O = U = L = N = R = S = T = 1
    D = G = 2
    B = C = M = P = 3
    F = H = V = W = Y = 4
    K = 5
    J = X = 8
    Q = Z = 10

def second_score(word):
    sum_score = 0
    for letter in word.upper():
        sum_score += Scrabble[letter]
        # a.k.a, sum_score += letter

    return sum_score