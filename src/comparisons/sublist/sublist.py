"""
This exercise stub and the test suite contain several enumerated constants.

Since Python 2 does not have the enum module, the idiomatic way to write
enumerated constants has traditionally been a NAME assigned to an arbitrary,
but unique value. An integer is traditionally used because it’s memory
efficient.
It is a common practice to export both constants and functions that work with
those constants (ex. the constants in the os, subprocess and re modules).

You can learn more here: https://en.wikipedia.org/wiki/Enumerated_type
"""

# Possible sublist categories.
# Change the values as you see fit.
SUBLIST = 0
SUPERLIST = 1
EQUAL = 2
UNEQUAL = 3


def sublist(list_one, list_two):
    """
    detemine 2 list relationship.
    :param list_one: list
    :param list_two: list
    :return: int
    """

    oneLen = len(list_one)
    twoLen = len(list_two)
    sub = 4
    if oneLen < twoLen:
        for i in list_one:
            if i in list_two:
                continue
            else:
                sub = UNEQUAL
                break
    elif oneLen > twoLen:
        for i in list_two:
            if i in list_one:
                continue
            else:
                sub = UNEQUAL
                break
    elif oneLen == twoLen:
        if list_one == list_two:
            sub = EQUAL
        else:
            sub = UNEQUAL
    else:
        sub = 5

    if sub == 4:
        strOne = "".join([str(i) for i in list_one])
        strTwo = "".join([str(i) for i in list_two])
        if strOne in strTwo:
            sub = SUBLIST
        elif strTwo in strOne:
            sub = SUPERLIST
        else:
            sub = 6

    if sub > 3:
        sub = UNEQUAL
    return sub
