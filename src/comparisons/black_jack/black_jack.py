"""Functions to help play and score a game of blackjack.

How to play blackjack:    https://bicyclecards.com/how-to-play/blackjack/
"Standard" playing cards: https://en.wikipedia.org/wiki/Standard_52-card_deck
"""


def value_of_card(card):
    """Determine the scoring value of a card.

    :param card: str - given card.
    :return: int - value of a given card.  See below for values.

    1.  'J', 'Q', or 'K' (otherwise known as "face cards") = 10
    2.  'A' (ace card) = 1
    3.  '2' - '10' = numerical value.
    """

    if card == 'A':
        valueOfCard = 1
    elif card in 'JQK' and len(card) == 1:
        valueOfCard = 10
    elif card in str(list(range(2, 11))):
        valueOfCard = int(card)
    return valueOfCard


def higher_card(card_one, card_two):
    """Determine which card has a higher value in the hand.

    :param card_one, card_two: str - cards dealt in hand.  See below for values.
    :return: str or tuple - resulting Tuple contains both cards if they are of equal value.

    1.  'J', 'Q', or 'K' (otherwise known as "face cards") = 10
    2.  'A' (ace card) = 1
    3.  '2' - '10' = numerical value.
    """

    if value_of_card(card_one) > value_of_card(card_two):
        higherCard = card_one
    elif value_of_card(card_one) < value_of_card(card_two):
        higherCard = card_two
    elif value_of_card(card_one) == value_of_card(card_two):
        higherCard = (card_one, card_two)
    return higherCard


def value_of_ace(card_one, card_two):
    """Calculate the most advantageous value for the ace card.

    :param card_one, card_two: str - card dealt. See below for values.
    :return: int - either 1 or 11 value of the upcoming ace card.

    1.  'J', 'Q', or 'K' (otherwise known as "face cards") = 10
    2.  'A' (ace card) = 11 (if already in hand)
    3.  '2' - '10' = numerical value.
    """

    if card_one in 'JQK' and card_two in 'JQK' and len(card_one) == 1 and len(card_two) == 1:
        valueOfAce = 1
    elif card_one in 'JQK' and card_two == 'A':
        valueOfAce = 1
    elif card_one in 'JQK' and len(card_one) == 1 and card_two in str(list(range(2, 11))):
        valueOfAce = 1
    elif card_one == 'A':
        valueOfAce = 1
    elif card_one in str(list(range(2, 11))) and card_two in str(list(range(2, 11))):
        if int(card_one) + int(card_two) <= 10:
            valueOfAce = 11
        else:
            valueOfAce = 1
    elif card_one in str(list(range(2, 11))) and card_two in 'JQK' and len(card_two) == 1:
        valueOfAce = 1
    elif card_one in str(list(range(2, 11))) and card_two == 'A':
        valueOfAce = 1
    else:
        raise ValueError("The input param has error about value of ace.")
    return valueOfAce


def is_blackjack(card_one, card_two):
    """Determine if the hand is a 'natural' or 'blackjack'.

    :param card_one, card_two: str - card dealt. See below for values.
    :return: bool - is the hand is a blackjack (two cards worth 21).

    1.  'J', 'Q', or 'K' (otherwise known as "face cards") = 10
    2.  'A' (ace card) = 11 (if already in hand)
    3.  '2' - '10' = numerical value.
    """

    if card_one == 'A' and value_of_card(card_two) == 10:
        isBlackJack = True
    elif value_of_card(card_one) == 10 and card_two == 'A':
        isBlackJack = True
    else:
        isBlackJack = False
    return isBlackJack


def can_split_pairs(card_one, card_two):
    """Determine if a player can split their hand into two hands.

    :param card_one, card_two: str - cards dealt.
    :return: bool - can the hand be split into two pairs? (i.e. cards are of the same value).
    """

    if card_one == card_two:
        canSplitPairs = True
    elif card_one in 'QK' and len(card_one) == 1 and card_two in 'QK' and len(card_two) == 1:
        canSplitPairs = True
    else:
        canSplitPairs = False
    return canSplitPairs


def can_double_down(card_one, card_two):
    """Determine if a blackjack player can place a double down bet.

    :param card_one, card_two: str - first and second cards in hand.
    :return: bool - can the hand can be doubled down? (i.e. totals 9, 10 or 11 points).
    """

    if value_of_card(card_one) + value_of_card(card_two) in range(9, 12):
        canDoubleDown = True
    else:
        canDoubleDown = False
    return canDoubleDown
