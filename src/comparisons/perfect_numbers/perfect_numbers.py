def classify(number):
    """ A perfect number equals the sum of its positive divisors.

    :param number: int a positive integer
    :return: str the classification of the input integer
    """

    if number <= 0:
        raise ValueError("Classification is only possible for positive integers.")

    divList = []
    for i in range(1, number):
        if number % i == 0:
            divList.append(i)

    if sum(divList) == number:
        strOutput = 'perfect'
    elif sum(divList) > number:
        strOutput = 'abundant'
    elif sum(divList) < number:
        strOutput = 'deficient'
    return strOutput
