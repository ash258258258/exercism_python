def score(x, y):
    """
    Write a function that returns the earned points in a single toss of a Darts game.
    """

    score = 0
    if 0 <= (x ** 2 + y ** 2) ** 0.5 <= 1:
        score = 10
    elif 1 < (x ** 2 + y ** 2) ** 0.5 <= 5:
        score = 5
    elif 5 < (x ** 2 + y ** 2) ** 0.5 <= 10:
        score = 1
    elif (x ** 2 + y ** 2) ** 0.5 >= 10:
        score = 0
    return score
