def is_pangram(sentence):
    """
    Determine if a sentence is a pangram.
    :param sentence: str
    :return: bool
    """

    letters = 'abcdefghijklmnopqrstuvwxyz'
    sentence = sentence.lower()

    for letter in letters:
        if letter in sentence:
            isPangram = True
            continue
        else:
            isPangram = False
            break
    return isPangram


# `all()` on lowercased letters
from string import ascii_lowercase


def second_is_pangram(sentence):
    return all(ltr in sentence.lower() for ltr in ascii_lowercase)


# `set` with `issubset()` on lowercased characters
from string import ascii_lowercase

ALPHABET = set(ascii_lowercase)


def third_is_pangram(sentence):
    return ALPHABET.issubset(sentence.lower())


# `set` with `len()` on lowercased characters
def four_is_pangram(sentence):
    return len([ltr for ltr in set(sentence.lower()) if ltr.isalpha()]) \
        == 26


# Bit field approach
# for a thirty-two bit integer, if the values for a and Z were both set, the bits would look like:
#       zyxwvutsrqponmlkjihgfedcba
# 00000010000000000000000000000001
A_LCASE = 97
A_UCASE = 65
ALL_26_BITS_SET = 67108863


def five_is_pangram(sentence):
    letters_flags = 0
    for letter in sentence:
        if letter >= 'a' and letter <= 'z':
            letters_flags |= 1 << ord(letter) - A_LCASE
        elif letter >= 'A' and letter <= 'Z':
            letters_flags |= 1 << ord(letter) - A_UCASE
    return letters_flags == ALL_26_BITS_SET
