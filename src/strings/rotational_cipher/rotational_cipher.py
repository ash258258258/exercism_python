# Using ASCII values
def f_rotate(text, key):
    """
    :param text: str
    :param key: int
    :return: str
    """

    rotateText = ""
    for digit in text:
        if digit.isalpha():
            if digit.isupper():
                rotateText += chr((ord(digit) - 65 + key) % 26 + 65)
            else:
                rotateText += chr((ord(digit) - 97 + key) % 26 + 97)
        else:
            rotateText += digit
    return rotateText



# Using recursion
# Caution: Python has a default recursion limit of 1000 calls on the stack
ALPHABET = "abcdefghijklmnopqrstuvwxyz"

def second_rotate(text, key):
    if text == "":
        return ""
    first_letter, rest = text[0], text[1:]
    result = ""

    if first_letter.lower() in ALPHABET:
        if first_letter.isupper():
            result = result + ALPHABET[(ALPHABET.index(first_letter.lower()) + key) % 26].upper() + rotate(rest, key)
        else:
            result = result + ALPHABET[(ALPHABET.index(first_letter) + key) % 26] + rotate(rest, key)
    else:
        result = result + first_letter + rotate(rest, key)
    
    return result


# Using ALPHABET index
AlPHABET = "abcdefghijklmnopqrstuvwxyz"

def rotate(text, key):
    result = ""
    for letter in text:
        if letter.isalpha():
            if letter.isupper():
                result += AlPHABET[(AlPHABET.index(letter.lower()) + key) % 26].upper()
            else:
                result += AlPHABET[(AlPHABET.index(letter) + key) % 26]
        else:
            result += letter
    return result