"""Functions for creating, transforming, and adding prefixes to strings."""


def add_prefix_un(word):
    """Take the given word and add the 'un' prefix.

    :param word: str - containing the root word.
    :return: str - of root word prepended with 'un'.
    """

    wordPred = 'un' + word
    return wordPred


def make_word_groups(vocab_words):
    """Transform a list containing a prefix and words into a string with the prefix followed by the words with prefix prepended.

    :param vocab_words: list - of vocabulary words with prefix in first index.
    :return: str - of prefix followed by vocabulary words with
            prefix applied.

    This function takes a `vocab_words` list and returns a string
    with the prefix and the words with prefix applied, separated
     by ' :: '.

    For example: list('en', 'close', 'joy', 'lighten'),
    produces the following string: 'en :: enclose :: enjoy :: enlighten'.
    """

    prefix = vocab_words[0]
    vocab = []
    for i in vocab_words:
        if i == vocab_words[0]:
            vocab.append(i)
            continue
        vocab.append(f'{prefix}{i}')
    separator = ' :: '
    vocabJoined = separator.join(vocab)
    return vocabJoined


def remove_suffix_ness(word):
    """Remove the suffix from the word while keeping spelling in mind.

    :param word: str - of word to remove suffix from.
    :return: str - of word with suffix removed & spelling adjusted.

    For example: "heaviness" becomes "heavy", but "sadness" becomes "sad".
    """

    wordPred = word[:-4]
    if wordPred.endswith('i'):
        wordPred = wordPred[:-1] + 'y'
    return wordPred


def adjective_to_verb(sentence, index):
    """Change the adjective within the sentence to a verb.

    :param sentence: str - that uses the word in sentence.
    :param index: int - index of the word to remove and transform.
    :return: str - word that changes the extracted adjective to a verb.

    For example, ("It got dark as the sun set", 2) becomes "darken".
    """

    splitSentence = sentence.split()
    wordSufs = splitSentence[index]
    if '.' in wordSufs:
        wordSufs = wordSufs[:-1]

    wordSufs = wordSufs + 'en'
    return wordSufs
