"""
Given a string the program should check if the provided string is a valid ISBN-10.
"""


def is_valid(isbn):
    """
    :param isbn: str
    :return: bool
    """

    isbn = isbn.replace("-", "")
    isbn9 = "0123456789"
    isbn10 = "0123456789X"
    isValid = True

    if len(isbn) != 10:
        isValid = False
    else:
        if isbn[-1] not in isbn10:
            isValid = False
        else:
            for i in isbn[0:-1]:
                if i not in isbn9:
                    isValid = False
                    break

    if not isValid:
        return isValid

    constant = 10
    isbnValue = 0
    if isbn[-1] == "X":
        isbnList = isbn[0:-1].replace("", " ").split()
        isbnList.append(10)
    elif isbn[-1] in isbn9:
        isbnList = isbn.replace("", " ").split()
    for item in isbnList:
        item = int(item)
        isbnValue += constant * item
        constant -= 1

    isbnValue = isbnValue % 11
    if isbnValue == 0:
        isValid = True
    else:
        isValid = False
    return isValid


def second_is_valid(isbn):
    nums = list(isbn.replace("-", ""))
    if len(nums) != 10:
        isValid = False
        return isValid
    if nums[-1] == "X":
        nums[-1] = "10"
    if not all(item.isdigit() for item in nums):
        isValid = False
        return isValid

    isbnValue = 0
    for i, j in zip(nums, range(10, 0, -1)):
        value = int(i) * j
        isbnValue += value
    isbnValue = isbnValue % 11
    if isbnValue == 0:
        isValid = True
    else:
        isValid = False
    return isValid
