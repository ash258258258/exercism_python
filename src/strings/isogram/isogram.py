def is_isogram(string):
    """
    Determine if a word or phrase is an isogram.
    :param string: str
    :return: bool
    """
    string = string.lower()
    isIsogram = True
    for i in string:
        for j in string:
            if i == j and j not in ['-', ' ']:
                if string.count(i) > 1:
                    isIsogram = False
                    break
    return isIsogram


# list comprehension
def second_is_isogram(phrase):
    scrubbed = [ltr.lower() for ltr in phrase if ltr.isalpha()]
    return len(set(scrubbed)) == len(scrubbed)


# comparising len
def third_is_isogram(phrase):
    scrubbed = phrase.replace('-', '').replace(' ', '').lower()
    return len(scrubbed) == len(set(scrubbed))


# scrub with `re.sub()`
import re


def fourth_is_isogram(phrase):
    scrubbed = re.compile('[^a-zA-Z]').sub('', phrase).lower()
    return len(set(scrubbed)) == len(scrubbed)


# filter with `re,findall()`
import re


def fiveth_is_isogram(phrase):
    scrubbed = "".join(re.findall("[a-zA-Z]", phrase)).lower()
    return len(set(scrubbed)) == len(scrubbed)
