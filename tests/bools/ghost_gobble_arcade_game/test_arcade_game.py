import unittest
from src.bools.ghost_gobble_arcade_game.arcade_game import (
    eat_ghost,
    score,
    lose,
    win
)


class TestArcadeGame(unittest.TestCase):

    def test_ghost_gets_eaten(self):
        self.assertIs(
            eat_ghost(True, True),
            True,
            msg="ghost should get eaten"
        )

    def test_ghost_does_not_get_eaten_cause_no_power_pellet_active(self):
        self.assertIs(
            eat_ghost(False, True),
            False,
            msg="ghost does not get eaten cause no power pellet active"
        )

    def test_ghost_not_get_eaten_cause_not_touching_ghost(self):
        self.assertIs(
            eat_ghost(True, False),
            False,
            msg="ghost does not get eaten cause not touching ghost"
        )

    def test_ghost_not_get_eaten_cause_no_power_pellet_active_and_not_touching_ghost(self):
        """
        extra.
        """
        self.assertIs(
            eat_ghost(False, False),
            False,
            msg="ghost does not get eaten cause no power pellet active and not touching ghost"
        )

    def test_score_when_eat_dot(self):
        self.assertIs(
            score(False, True),
            True,
            msg="score when eating dot"
        )

    def test_score_when_eating_power_pellet(self):
        self.assertIs(
            score(True, False),
            True,
            msg="score when eating power pellet"
        )

    def test_no_score_when_nothing_eaten(self):
        self.assertIs(
            score(True, False),
            True,
            msg="no score when nothin eaten"
        )

    def test_lose_if_touching_a_ghost_without_a_power_pellet(self):
        self.assertIs(
            lose(False, True),
            True,
            msg="lose if touching a ghost without a power pellet active"
        )

    def test_dont_lose_if_touching_a_ghost_with_a_power_pellet_active(self):
        self.assertIs(
            lose(True, True),
            False,
            msg="don't lose if touching a ghost with a power pellet active"
        )

    def test_dont_lose_if_not_touching_a_ghost(self):
        self.assertIs(
            lose(True, False),
            False,
            msg="don't lose if not touching a ghost"
        )

    def test_win_if_all_dots_eaten(self):
        self.assertIs(
            win(True, False, False),
            True,
            msg="win if all dots eaten"
        )

    def test_dont_win_if_all_fots_eaten_but_touching_a_ghost(self):
        self.assertIs(
            win(True, False, True),
            False,
            msg="don't win if all dots eaten, but touching a ghost"
        )

    def test_dont_win_if_not_all_dots_eaten(self):
        self.assertIs(
            win(False, True, True),
            False,
            msg="don't win if not all dots eaten and touching a ghost with a power pellet active"
        )


if __name__ == '__main__':
    unittest.main()
